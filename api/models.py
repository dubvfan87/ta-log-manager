import time, datetime
from django.db import models

class LogItem(models.Model):
    username = models.CharField(max_length=300)
    text = models.TextField()
    timestamp = models.DateTimeField()
    
    def timestamp_as_epoch(self):
        return int(time.mktime(self.timestamp.timetuple())*1000)
    
    @staticmethod
    def get_last_sync_time():
      try:
        item = LogItem.objects.latest(timestamp)
        return item.timestamp
      except NameError:
        return None
      

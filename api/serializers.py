from rest_framework import serializers
from rest_framework import pagination
from .models import LogItem

class LogItemSerializer(serializers.ModelSerializer):
    unix_timestamp_utc = serializers.Field(source='timestamp_as_epoch')
    class Meta:
        model = LogItem
        fields = ('username', 'text', 'timestamp', 'unix_timestamp_utc')
        
class PaginatedLogItemSerializer(pagination.PaginationSerializer):
    """
    Serializes page objects of user querysets.
    """
    class Meta:
        object_serializer_class = LogItemSerializer

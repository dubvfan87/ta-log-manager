from django.shortcuts import render
from .models import LogItem
from .serializers import LogItemSerializer, PaginatedLogItemSerializer
from rest_framework import filters
from rest_framework import generics
        
class LogItemList(generics.ListCreateAPIView):
    serializer_class = PaginatedLogItemSerializer
    filter_backends = (filters.DjangoFilterBackend,filters.OrderingFilter,filters.SearchFilter,)    
    ordering = ('-timestamp',)
    search_fields = ('username', 'message')
    paginate_by = 500
    paginate_by_param = 'limit'
    max_paginate_by = 1000
    serializer_class = LogItemSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a 'username' query parameter in the URL.
        """
        queryset = LogItem.objects.all()
        username = self.request.QUERY_PARAMS.get('username', None)
        limit = self.request.QUERY_PARAMS.get('username', None)
        if username is not None:
            queryset = queryset.filter(purchaser__username=username)            
        if limit is not None:
            queryset = queryset[:limit]
            
        return queryset

import os,re,pytz
from django.utils import timezone
from django.conf import settings
from .models import *

from datetime import datetime

def sync_log_file_with_db():      
    last_sync = LogItem.get_last_sync_time()    
    now = timezone.localtime(timezone.now())
        
    # build path for today's log
    log_path = os.path.join(settings.EGGDROP_LOG_DIR)    
    log_file = '%s%s' % (settings.EGGDROP_LOG_FILE_PREFIX,
                           now.strftime("%d%b%Y"),)
    
    full_path_to_log_file = os.path.join(log_path, log_file)
      
    # turn log file into a list    
    log_file_list = open(full_path_to_log_file, 'r')
    
    for log in log_file_list:
      # remove new lines
      log = log.replace('\n', '')
      
      try:      
        time = re.search('\[(.)*\]', log).group(0).replace('[', '').replace(']', '')
        username = re.search('\<.*\>', log).group(0).replace('>', '').replace('<', '')
        message = re.search('\>\s.*', log).group(0).replace('>', '').strip()
          
        # create python datetime
        string_time = now.strftime('%d%b%Y') + ' ' + time
        python_time = datetime.strptime(string_time, '%d%b%Y %H:%M:%S').replace(tzinfo=pytz.timezone(settings.TIME_ZONE))
      
        if LogItem.objects.filter(username=username, text=message, timestamp=python_time).count() == 0:
          LogItem.objects.create(username=username,
                                 text=message,
                                 timestamp=python_time,)
                                 
      except AttributeError: # non-user message
        pass

from django.conf.urls import patterns, include, url
from rest_framework.urlpatterns import format_suffix_patterns
from api.views import *

urlpatterns = patterns('api.urls',
                       url(r'^logs/?$', LogItemList.as_view()),
                       )

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'html'])
